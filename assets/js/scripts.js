$(document).ready(function(){
  $('.aviso').hide();
  paginacion();
  comicsAleatorios();
});

//Función para agregar nuevos comics a la lista de favoritos
$(document).on('click', ".agregar", function(){
  var id= $(this).attr('id');
  var dir=$(this).attr('value');
  var local= JSON.parse(localStorage.getItem("jsonData"));
  $.get("/comic", { uri: dir} , function(data){
  console.log( dir);
  var existe;
      for (var i = 0; i < local.comics.length; i++) {
        if (local.comics[i].resourceURI==dir) {
          existe='true';
        }
      }

      if (existe=='true') {
        alert("No se puede añadir el comic selecionado a favoritos porque ya se encuentra en la lista")
      }
      else {
            localStorage.clear();
            local.comics.push({
              "title": data.data.results[0].title,
              "thumbnail": data.data.results[0].thumbnail,
              "resourceURI":data.data.results[0].resourceURI
            });
            localStorage.setItem("jsonData", JSON.stringify(local));
            cargarlocalStorage();
      }
    }).fail( function () {
      console.log("Error");
    })


});

// Funcion boton de eliminar comic
$(document).on('click', ".remove", function(){
  var id= $(this).attr('id');
  var dir=$(this).attr('value');
  var local= JSON.parse(localStorage.getItem("jsonData"));
  for (var i = 0; i < local.comics.length; i++) {
    if (local.comics[i].resourceURI==dir) {
        localStorage.clear();
        local.comics.splice(i, 1);
        console.log(  local.comics);
        localStorage.setItem("jsonData", JSON.stringify(local));
    }
  }
  cargarlocalStorage();
});

//Realiza la paginacion de los personajes y arma la vista Realizando la busqueda con GetURLParameter('offset', offset);
function paginacion() {
    var offset=0;
    var pagtotales= Math.ceil($('#pagination').attr('value'));
    $('#pagination').twbsPagination({
      totalPages: pagtotales,
      visiblePages: 10,
      cssStyle: "compact-theme",
      initiateStartPageClick: false,
      onPageClick: function (event, page) {
        offset = 10 *( $('.active').text()-1);
        GetURLParameter('offset', offset);
      }
    });
};

//Abre el modal del personaje
$(".modalCharacter").click(function(event){
  event.preventDefault();
  var idCharacter= $(this).val();
  $.get("/character", { id:  idCharacter} , function(data){
    $('#comics').html('');
    $('#series').html('');
    $('#events').html('');
    $('#stories').html('');
    var html = "<img  class='img-responsive img-circle'  src= " +data.data.results[0].thumbnail.path + '/standard_xlarge.'+ data.data.results[0].thumbnail.extension +">";
    $('#box36').html(html);
    $('#box32').html("<img  class='fondo' src="+data.data.results[0].thumbnail.path + "."+ data.data.results[0].thumbnail.extension+">");
    $('#nameCharacter').html(data.data.results[0].name);
    if (data.data.results[0].description){
      $('#descriptionCharacter').html(data.data.results[0].description);
    }
    else {
      $('#description').html("<BR><BR>");
    }
    //Tarea URL de asociada a los personajes
    var numeroUrl= data.data.results[0].urls.length;
    var urlComic= '';
    var url = " <img src='../assets/img/icons/favourites.png'><titulo style=' margin-right: 4em;'> Read More on:</titulo> <a   href=" +data.data.results[0].urls[0].url +"  target=”_blank”>Marvel Universe</a>";
    if(numeroUrl==2){
      urlComic +="<a href=" +data.data.results[0].urls[1].url +"  target=”_blank”>Search more in Marvel about</a>";
    }
   else {
      url += "<Br><div  align='right' style=' padding-right: 30px;'><a href=" +data.data.results[0].urls[1].url +"  target=”_blank”>Marvel Universe WIKI</a></div>";
      urlComic +="<a  href=" +data.data.results[0].urls[2].url +"  target=”_blank”>Search more about..</a>";
    }
    $('#comic').html(urlComic);
    $('#box33').html(url);

    //Lista de comics asociados al personaje
    if (data.data.results[0].comics.items.length>0) {
      var html ="<titulo>Comics: </titulo>";
      html +="<div class='style-1' style='height:90px; overflow: auto; 	margin-left: 2em;margin-top 1em;'>";
      for( var i = 0; i < data.data.results[0].comics.items.length ; i++) {
        if (data.data.results[0].comics.items[i]){
            html +="<texto><a href='#' id='comicAdd' class='modalComic' value="+data.data.results[0].comics.items[i].resourceURI+">"+data.data.results[0].comics.items[i].name+"</a></texto><br>"
          }
      }
      html +="</div>";
      $('#comics').html(html);
    }

    //Lista de series asociados al personaje
    if (data.data.results[0].series.items.length>0) {
      var html ="<titulo>Series: </titulo>";
      html +="<div class='style-1' style='height:90px; overflow: auto; 	margin-left: 2em;margin-top 1em;'>";
        for( var i = 0; i < data.data.results[0].series.items.length; i++) {
          if (data.data.results[0].series.items[i]){
            html +="<texto>"+data.data.results[0].series.items[i].name+"</texto><Br>"
          }
        }
      html +="</div>";
      $('#series').html(html);
    }

    //Lista de stories asociados al personaje
    if (data.data.results[0].stories.items.length>0) {
      var html ="<titulo>Stories: </titulo>";
      html +="<div class='style-1' style='height:90px; overflow: auto; 	margin-left: 2em;margin-top 1em;'>";
        for( var i = 0; i < data.data.results[0].stories.items.length; i++) {
          if (data.data.results[0].stories.items[i]){
            html +="<texto>"+data.data.results[0].stories.items[i].name+"</texto><Br>"
          }
        }
      html +="</div>";
      $('#stories').html(html);
    }

    //Lista de events asociados al personaje
    if (data.data.results[0].events.items.length>0) {
      var html ="<titulo>Events: </titulo>";
      html +="<div class='style-1' style='height:90px; overflow: auto; 	margin-left: 2em;margin-top 1em;'>";
        for( var i = 0; i < data.data.results[0].events.items.length; i++) {
          if (data.data.results[0].events.items[i]){
            html +="<texto>"+data.data.results[0].events.items[i].name+"</texto><Br>"
          }
        }
      html +="</div>";
      $('#events').html(html);
    }
    $('#myModal2').modal('show');
  }).fail( function () {
    console.log("Error");
  })
});

//Abre el modal del comic
$(document).on('click', ".modalComic",function(event){
  var id= $(this).attr('id');
   $('#myModal2').modal('hide');
  event.preventDefault();
  var uri= $(this).attr('value');
  $.get("/comic", { uri: uri} , function(data){
    $('#name').html('');
    $('#description').html('');
    $('#name').html(data.data.results[0].title);
    $('#description').html(data.data.results[0].description);
    var html= "<img src="+ data.data.results[0].thumbnail.path + '/portrait_incredible.'+data.data.results[0].thumbnail.extension+">"
    $('#box11').html(html);
    if (data.data.results[0].prices.length>0) {
      for( var i=0; i<data.data.results[0].prices.length; i++){
        if (data.data.results[0].prices[i].type == "digitalPurchasePrice") {
          var price= " <img src=../assets/img/icons/shopping-cart-primary.png>&nbsp&nbsp<a type=image  target=”_blank” href="+data.data.results[0].urls[0].url+ ">BUY  FOR "+data.data.results[0].prices[i].price+"</a>";
        }
        else {
          var price= " <img src=../assets/img/icons/shopping-cart-primary.png>&nbsp&nbsp<a type=image  target=”_blank” href="+data.data.results[0].urls[0].url+ ">BUY</a>";
        }
      }
    }
    $('#box23').html(price);
    if (id=='comicAdd') {
      var html= "<img src=../assets/img/icons/btn-favourites-default.png> &nbsp&nbsp <a class='cafe agregar' data-dismiss='modal' href='#' value='"+data.data.results[0].resourceURI+"'>ADD TO FAVOURITES</a>"
      $('#box21').hide();
      $('#box22').show();
      $('#box22').html(html);
    }

    if (id=='comicADDED') {
      console.log("entré a comic añladido");
      var html="<img src=../assets/img/icons/btn-favourites-primary.png>&nbsp&nbsp <a data-dismiss='modal'  href='#' >ADDED TO FAVOURITES</a>";
      $('#box21').show();
      $('#box22').hide();
      $('#box21').html(html);
    }

    $('#myModal').modal('show');
  }).fail( function () {
    console.log("Error");
  })
});

//Busca personajes por letra, letras o nombre completo haciendo un llamado a GetURLParameter('name', name);
$(".buscar").click(function(){
  event.preventDefault();
  var nombre= $(".form-control").val();
  var res = nombre.split(" ");
  var name='';
  if ( res.length>1) {
    for (var i = 0; i < res.length; i++) {
      if (i==(res.length-1)) {
          name+=res[i];
      }
      else{
        name+=res[i]+'-'
      }
    }
    GetURLParameter('name', name);
  }
  if ( res.length==1) {
     name=res[0];
     GetURLParameter('name', name);
  }
});

//Ordena los ppersonajes en el index, llama la función GetURLParameter('orden', idorden)
$(".ordenar").change(function(event){
  event.preventDefault();
  var idorden= $(".Sort").val();
  var parametro =$(location).attr('search');
  GetURLParameter('orden', idorden);
});

//Estructura todas las direcciones con los parametros para redireccionar el index con la nueva busqueda
function GetURLParameter(sParam, param) {
    var params = [];
    var sPageURL = window.location.search.substring(1);
    var sURLVariables = sPageURL.split('&');
    var direccion ='/?';
    var control =0;
    for (var i = 0; i < sURLVariables.length; i++)
    {
        var sParameterName = sURLVariables[i].split('=');
        if (sParameterName[0] == sParam){
            sParameterName[1]=param;
            control=1;
        }
        if ((sParam=='name' || sParam=='orden') && sParameterName[0] == 'offset') {
          sParameterName[1]=0
        }
        params[i] = sParameterName;
    }
    if (control==0) {
      params.push([sParam,param]);
    }
    for (var i = 0; i < params.length; i++) {
      if (i==0) {
        direccion +=params[i][0]+ "="+params[i][1]
        console.log(params[i][0]+ "="+params[i][1]);
      }
      else {
        direccion +="&"+params[i][0]+ "="+params[i][1]
      }
      }
      window.location.href = direccion
}

//Crea los numeros aleatorios para realizar la busqueda de comics aleatorios
function  randomGenerator(a, hasta) {
  var v = Math.floor(Math.random() * (hasta - 1) + 1);
  if(!a.some(function(e){return e == v})){
      a.push(v);
  }
}

//Crea los 3 comics aleatorios y llama a cargarlocalStorage() y randomGenerator(arr,hasta)
function comicsAleatorios(){
        var local= JSON.parse(localStorage.getItem("jsonData"));
        var obj = {};
        obj.comics=[];
        if(!local){
          localStorage.setItem("jsonData", JSON.stringify(obj));
          local= JSON.parse(localStorage.getItem("jsonData"));
        }
        var arr = [];
        var hasta=19;
        while(arr.length < 3 && 3 < hasta){
          randomGenerator(arr,hasta);
        }
       if (local.comics.length==0 ) {
        $.get("/comic", { uri: 'https://gateway.marvel.com:443/v1/public/comics'} , function(data){
          for (var i = 0; i < arr.length; i++) {
            obj.comics.push({
              "title": data.data.results[arr[i]].title,
              "thumbnail": data.data.results[arr[i]].thumbnail,
              "resourceURI":data.data.results[arr[i]].resourceURI
            });
          }
          localStorage.setItem("jsonData", JSON.stringify(obj));
          cargarlocalStorage();
        }).fail( function () {
          console.log("Error");
        })
      }
      else {
        cargarlocalStorage();
      }
}

//Carga los datos del localStorage en la pagina
function cargarlocalStorage(){
  var html='';
  var local= JSON.parse(localStorage.getItem("jsonData"));
  if (local.comics.length!=0) {
      for (var i = 0; i < local.comics.length; i++) {
        html+="<BR><BR><div align='center' id='Eliminadocomic"+[i]+"'>"
        html+="<div class='image_wrapper'>"
        html+= "<img src= "+local.comics[i].thumbnail.path + '/portrait_incredible.'+local.comics[i].thumbnail.extension+" class='image'>"
        html+="<img src='../assets/img/icons/btn-delete.png' title='eliminar' id='comic"+i+"' class='remove' value="+local.comics[i].resourceURI+">"
        html+="</div>"
        html+="<a href='#' class='modalComic' id='comicADDED' value="+local.comics[i].resourceURI+" id='cafe' >"+local.comics[i].title+"</a>"
        html+="</div>"
      }
      $('#favouritesComics').html(html);
  }
  else {
    comicsAleatorios();
  }
}
